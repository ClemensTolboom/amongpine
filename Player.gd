extends KinematicBody2D

var moveSpeed : int = 250

var vel : Vector2 = Vector2()
var facingDir : Vector2 = Vector2(-1, 0)


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _physics_process(delta):
	vel = Vector2()
	
	# inputs
	if Input.is_action_pressed("move_up"):
		vel.y -= 1
		
	if Input.is_action_pressed("move_down"):
		vel.y += 1
		
	if Input.is_action_pressed("move_left"):
		vel.x -= 1
		facingDir = Vector2(-1, 0)
		
	if Input.is_action_pressed("move_right"):
		vel.x += 1
		facingDir = Vector2(1, 0)
	
	vel = vel.normalized()
	
	# move the player
	move_and_slide(vel * moveSpeed)
